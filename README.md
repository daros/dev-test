# README #

### How do I get set up? ###
* the application is runnable as an executable .jar, from parent directory run:
* java -jar dev-test.jar
* access application at localhost:8080

### Running the tests ###
* remember to stop the executable jar before trying to build the project
* import as existing maven project
* maven update
* build parent project with: mvn clean install
* backend tests can be ran separately as junit tests from backend project
* if you install node and yarn you can run frontend tests separately with: yarn test 
