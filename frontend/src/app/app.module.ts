import { NgModule }	 				from '@angular/core';
import { BrowserModule } 	from '@angular/platform-browser';
import { FormsModule }  	 from '@angular/forms';
import { HttpModule } 		from '@angular/http';

import { AppComponent }		 from './app.component';
import { SortModule } 		from './sort/sort.module';
import { SortComponent }	from './sort/sort.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    SortModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
