import { NgModule }	 				from '@angular/core';
import { CommonModule } 			from '@angular/common';
import { FormsModule }    			from '@angular/forms';
import { HttpModule } 				from '@angular/http';

import { SortComponent }			from './sort.component';
import { SortService, SortResult } 	from './sort.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
  	SortComponent
  ],
  providers: [ SortService ],
  exports: [SortComponent]
})
export class SortModule { }