import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { SortService, SortResult} from './sort.service';


@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.css']
})

export class SortComponent{

	results : Observable<SortResult[]>;
	error : string;

	constructor(private service: SortService){
		this.loadResults();
	}

	loadResults(){
		this.results = this.service.getSortResults();
	}

    sortInput(input:string){
	    this.service.executeSort(input).subscribe(res => {
		    	this.loadResults();
		    	this.resetError();
		    },
		    err => {
		    	this.resetError();
		    	let error = err.json();
		    	for(let i=0;i<error.errors.length;i++){
		    		if(error.errors[i]){
		    			console.log(error.errors[i].defaultMessage);
		    	 		this.error = this.error + error.errors[i].defaultMessage + "; ";
		    	 	}
		    	}
		    });
	}

	resetError(){
		this.error = '';
	}
}