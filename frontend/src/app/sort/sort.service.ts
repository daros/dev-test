import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

export class SortResult{
  constructor(
    public id: number,
    public original: string,
    public sorted: string,
    public sortTime: number,
    public sortSwaps: number,
    ){}
}

export class SortPayload{
  constructor(
    public sortInput: string,
    ){}
}

@Injectable()
export class SortService {
  // Resolve HTTP using the constructor
  constructor (private http: Http) {}

  private sortUrl = 'http://localhost:8080/api/sort';

  getSortResults() :Observable<SortResult[]>{
    return this.getRequest(this.sortUrl).map(this.extractData);
  }

  executeSort(unsortedInput:string){
  	let payload = new SortPayload(unsortedInput);
    return this.postRequest(this.sortUrl, payload)
    	.map(this.extractData)
  }

  getRequest(url:string): Observable<Response>{
    return this.http.get(url, this.createHeaders());
  }

  postRequest(url:string, body:any): Observable<Response>{
    return this.http.post(url, JSON.stringify(body), this.createHeaders());
  }

  extractData(res:Response){
    var json = res.json();
    return json as SortResult[];
  }

  createHeaders(){
    let headers = new Headers();
    headers.append("Access-Control-Allow-Origin", "*")
    headers.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    headers.append('Content-Type', 'application/json');
    return new RequestOptions({headers:headers});
  }
}