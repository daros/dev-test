import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { SortModule } from './sort/sort.module';
import {HttpModule} from '@angular/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        SortModule
      ], 
      declarations: [
        AppComponent
      ],
      providers:[]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'H&M Devtest'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('H&M Devtest');
  }));
});
