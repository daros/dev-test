package com.dros.app.api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.dros.app.api.model.SortResult;

public interface SortRepository extends CrudRepository<SortResult, Long> {

	List<SortResult> findAllByOrderByIdDesc();
}
