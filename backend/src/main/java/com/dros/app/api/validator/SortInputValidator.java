package com.dros.app.api.validator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SortInputValidator implements ConstraintValidator<SortInput, String>{

	@Override
	public void initialize(SortInput context) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean isValid(String input, ConstraintValidatorContext ctx) {
		
		if(input == null || input.isEmpty()){
			ctx.buildConstraintViolationWithTemplate("No input provided!").addConstraintViolation();
			return false;
		}
		
		List<String> inputList = Arrays.asList(input.split(","));
		
		try{
			List<Integer> numbers = inputList.stream().map(str -> Integer.valueOf(str.trim())).collect(Collectors.toList());
			
			if(validateCollectionSize(numbers, ctx) && validateNumberRange(numbers,ctx)){
				return true;
			}
			return false;
		}catch(NumberFormatException nfe){
			ctx.buildConstraintViolationWithTemplate("the input did not consist of numbers only").addConstraintViolation();
			return false;
		}		
	}

	private boolean validateNumberRange(List<Integer> numbers, ConstraintValidatorContext ctx ){
		for(Integer number : numbers){
			if(number < 0 || number > 100){
				ctx.buildConstraintViolationWithTemplate("Provided numbers must be between 0-100").addConstraintViolation();
				return false;
			}
		}
		return true;
	}
	
	private boolean validateCollectionSize(List<Integer> numbers, ConstraintValidatorContext ctx){
		if(numbers.size() > 50){
			ctx.buildConstraintViolationWithTemplate("provided collection is too big").addConstraintViolation();
			return false;
		} 
		return true;
	}
}
