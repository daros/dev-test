package com.dros.app.api.sort;

import java.util.List;

import com.dros.app.api.model.SortResult;

public class QuickSortImpl implements SortMethod{
	
	private List<Integer> sortedList;
    private long swaps = 0;
    private long time;

	@Override
	public SortResult doSort(List<Integer> listToSort) {
		SortResult result = new SortResult();
		result.setOriginal(listToSort);
		
		sort(listToSort);
		
		result.setSorted(sortedList);
		result.setSortTime(time);
		result.setSortSwaps(swaps);
		
		return result;
	}

	public void sort(List<Integer> input) {
        
        if (input == null || input.isEmpty()) {
            return;
        }
        this.sortedList = input;
        
        long start = System.nanoTime();
        quickSort(0, input.size()-1);
        long duration = System.nanoTime() - start;
        this.time = duration / 1000; //convert to microseconds        
    }
 
    private void quickSort(int lowestIndex, int highestIndex) {
        int low = lowestIndex; //checks left to right
        int high = highestIndex; //checks right to left
        
        //for pivot point we get the value in the middle
        int pivot = sortedList.get(lowestIndex+(highestIndex-lowestIndex)/2);
        while (low <= high) {
            while (sortedList.get(low) < pivot) {
                low++;
            }
            while (sortedList.get(high) > pivot) {
                high--;
            }
            if (low <= high) {
                swapNumbers(low, high);
                low++;
                high--;
            }
        }        
        if (lowestIndex < high)
        	//recursive call
            quickSort(lowestIndex, high);
        if (low < highestIndex)
        	//recursive call
        	quickSort(low, highestIndex);
    }
 
    private void swapNumbers(int low, int high) {
        int lowPosition = sortedList.get(low);
        sortedList.set(low, sortedList.get(high));
        sortedList.set(high, lowPosition);
        swaps++;
    }
}
