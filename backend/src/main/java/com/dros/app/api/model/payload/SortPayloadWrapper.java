package com.dros.app.api.model.payload;

import java.util.List;

import com.dros.app.api.validator.SortInput;

public class SortPayloadWrapper {
	
	@SortInput
	private String sortInput;
	
	public SortPayloadWrapper(){}

	public String getSortInput() {
		return sortInput;
	}

	public void setSortInput(String sortInput) {
		this.sortInput = sortInput;
	}

}
