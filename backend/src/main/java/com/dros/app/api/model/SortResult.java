package com.dros.app.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SORT_RESULT")
public class SortResult {
	
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name = "ORIGINAL")
	private String original;
	
	@Column(name = "SORTED")
	private String sorted;
	
	//measured in microseconds
	@Column(name = "SORT_TIME")
	private Long sortTime;
	
	@Column(name = "SORT_SWAPS")
	private Long sortSwaps;

	public SortResult(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}
	
	public void setOriginal(List<Integer> original){
		this.original = original.toString().replace("[", "").replace("]", "");
	}

	public String getSorted() {
		return sorted;
	}

	public void setSorted(String sorted) {
		this.sorted = sorted;
	}
	
	public void setSorted(List<Integer> sorted){
		this.sorted = sorted.toString().replace("[", "").replace("]", "");
	}

	public Long getSortTime() {
		return sortTime;
	}

	public void setSortTime(Long sortTime) {
		this.sortTime = sortTime;
	}

	public Long getSortSwaps() {
		return sortSwaps;
	}

	public void setSortSwaps(Long sortSwaps) {
		this.sortSwaps = sortSwaps;
	}
	
	
}
