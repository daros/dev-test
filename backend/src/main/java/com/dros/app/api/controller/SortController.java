package com.dros.app.api.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dros.app.api.model.SortResult;
import com.dros.app.api.model.payload.SortPayloadWrapper;
import com.dros.app.api.service.SortService;

@RestController
public class SortController {

	@Autowired 
	SortService service;
	
    @GetMapping(path = "/sort")
    public List<SortResult> findAllSortResults() {
        return service.findAllSortResults();
    }
    
    @PostMapping(path ="/sort")
    @ResponseStatus(HttpStatus.CREATED)
    public SortResult sortAndSave(@RequestBody @Valid SortPayloadWrapper payload){
    	List<String> split = Arrays.asList(payload.getSortInput().split(","));
		List<Integer> numbers = split.stream().map(str -> Integer.valueOf(str.trim())).collect(Collectors.toList());
    	return service.sortNumbersAndSave(numbers);
    }
}
