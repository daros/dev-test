package com.dros.app.api.validator;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SortInputValidator.class)
public @interface SortInput {
	String message() default "{com.dros.app.api.SortInput.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
