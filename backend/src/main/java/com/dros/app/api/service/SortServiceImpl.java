package com.dros.app.api.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dros.app.api.model.SortResult;
import com.dros.app.api.repository.SortRepository;
import com.dros.app.api.sort.QuickSortImpl;
import com.dros.app.api.sort.SortMethod;

@Service
@Transactional
public class SortServiceImpl implements SortService {
	
	@Autowired
	SortRepository repository;

	public List<SortResult> findAllSortResults() {
		
		return (List<SortResult>) repository.findAllByOrderByIdDesc();
	}

	@Override
	public SortResult sortNumbersAndSave(List<Integer> unsorted) {
		return repository.save(doSort(unsorted, new QuickSortImpl()));
	}

	@Override
	public SortResult doSort(List<Integer> unsorted, SortMethod method) {
		return method.doSort(unsorted);
	}	

}
