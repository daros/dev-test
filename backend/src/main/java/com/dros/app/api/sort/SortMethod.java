package com.dros.app.api.sort;

import java.util.List;

import com.dros.app.api.model.SortResult;

public interface SortMethod {
	
	SortResult doSort(List<Integer> unsorted);

}
