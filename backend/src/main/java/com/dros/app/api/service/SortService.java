package com.dros.app.api.service;

import java.util.List;

import com.dros.app.api.model.SortResult;
import com.dros.app.api.sort.SortMethod;

public interface SortService {
	List<SortResult> findAllSortResults();
	
	SortResult sortNumbersAndSave(List<Integer> unsorted);

	SortResult doSort(List<Integer> unsorted, SortMethod method);
}
