package com.dros.app.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.dros.app.api.model.SortResult;
import com.dros.app.api.repository.SortRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PersistenceTests {
	
	@Autowired
	SortRepository sortRepository;
	
	@Test
	public void whenFindByName_thenReturnEmployee() {
	    // given
	    SortResult result = new SortResult();
	    result.setOriginal("4,3,2,1");
	    result.setSorted("1,2,3,4");
	    result.setSortTime(23L);
	    result.setSortSwaps(6L);
	    result = sortRepository.save(result);
	 
	    // when
	    SortResult found = sortRepository.findOne(result.getId());
	 
	    // then
	    assertTrue("should be equal", found.getSorted().equals(result.getSorted()));
	}

}
