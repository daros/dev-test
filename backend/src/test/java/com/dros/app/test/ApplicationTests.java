package com.dros.app.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintValidatorContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.dros.app.api.model.SortResult;
import com.dros.app.api.sort.QuickSortImpl;
import com.dros.app.api.sort.SortMethod;
import com.dros.app.api.validator.SortInputValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	ConstraintValidatorContext ctx;
	
	@Before
	public void prepareTestEnv(){
		ctx = Mockito.mock(ConstraintValidatorContext.class, Mockito.RETURNS_DEEP_STUBS);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testQuickSort() throws Exception{
		Map<String, List<Integer>> testCases = new HashMap<String, List<Integer>>();
		testCases.put("1, 12, 13, 14, 32, 51, 51, 61, 71", Arrays.asList(1,32,14,51,61,71,12,13,51));
		testCases.put("1, 2, 3, 4, 5, 6, 7, 8, 9", Arrays.asList(1,2,3,4,5,6,7,8,9));
		testCases.put("1, 2, 3, 4, 5, 6, 7, 8, 9", Arrays.asList(9,8,7,6,5,4,3,2,1));
		testCases.put("1, 2, 3, 4, 5, 6, 7, 8, 9", Arrays.asList(4,1,7,2,9,5,3,6,8));
		testCases.put("3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3", Arrays.asList(3,3,3,3,3,3,3,3,3,3,3));
		testCases.put("1, 6, 10, 11, 12, 14, 15, 20, 22, 23, 25, 26, 27, 32, 32, 41, 42, 43, 54, 55, 64, 65, 78, 78, 90, 95, 100", 
				Arrays.asList(12,32,11,54,100,42,78,43,22,1,95,26,10,90,27,23,14,55,64,41,6,20,25,32,15,65,78));
		
		testCases.forEach((expected,input) -> doQuickSort(input, expected));
	}
	
	private void doQuickSort(List<Integer> input, String expected){
		SortMethod method = new QuickSortImpl();
		SortResult result = method.doSort(input);
		assertEquals("list is not sorted correctly", expected, result.getSorted());
		assertNotNull("original should not be empty", result.getOriginal());
		assertNotNull("swaps should not be empty", result.getSortSwaps());
		assertNotNull("time should not be empty", result.getSortTime());
	}
	
	@Test
	public void testInputValidation(){
		String validInput = "1, 2, 3, 53, 16, 94, 100, 0";
		String noInput = "";
		String nullInput = null;
		String tooBigCollection = "15, 65, 78, 12, 43, 25, 32, 15, 65, 78, 12, 43, 25, 32, 15, 65, 78, 12, 43, 25, 32, 15, 65, 78, 12, 43, 25, 32, 15, 65, 78, 12, 43, 25, 32, 15, 65, 78,15, 65, 78, 12, 43, 25, 32, 15, 65, 78, 12, 43, 25, 32, 15, 65, 78, 12, 43, 25, 32, 15, 65, 78, 12, 43, 25, 32, 15, 65, 78, 12, 43, 25, 32, 15, 65, 78";
		String negativeNumbers = "-1, -2, -4, 34, 16, 15";
		String tooBigNumbers = "102,1540,321,161";
		SortInputValidator validator = new SortInputValidator();
		
		assertTrue("should be true", validator.isValid(validInput, ctx));
		
		assertFalse("should be false", validator.isValid(noInput, ctx));
		assertFalse("should be false", validator.isValid(nullInput, ctx));
		assertFalse("should be false", validator.isValid(tooBigCollection, ctx));
		assertFalse("should be false", validator.isValid(tooBigNumbers, ctx));
		assertFalse("should be false", validator.isValid(negativeNumbers, ctx));
	}
}
